FROM node

RUN mkdir /devops
WORKDIR /devops
COPY package.json /devops
RUN yarn install

COPY . /devops
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
